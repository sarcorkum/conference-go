import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # use the Pexels API
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    # location = {"query": [str(Location.city), str(Location.state)]}
    url = f"https://api.pexels.com/v1/search?query={city}, {state}"
    # Make the request
    response = requests.get(url, headers=headers)
    # Parse the JSON response
    picture = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    picture_url = {"picture_url": picture["photos"][0]["url"]}
    return picture_url


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    geocode_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    # Make the request
    response = requests.get(geocode_url, params=params)
    # Parse the JSON response
    coordinates = json.loads(response.content)
    # Get the latitude and longitude from the response
    try:
        lon = coordinates[0]["lon"]
        lat = coordinates[0]["lat"]
    except (KeyError, IndexError):
        return None
    # Create the URL for the current weather API with the latitude
    #   and longitude
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    # Make the request
    response = requests.get(weather_url, params=params)
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    try:
        return {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
    except (KeyError, IndexError):
        return None
    # Return the dictionary
